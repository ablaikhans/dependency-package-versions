Use this script with parameters: 
    First parameter is "action". Available actions: getInstalledDependency, checkUpdates
    Second parameter is "dependency package name"
    Third parameter is "refresh repo metadata before updates check" - refresh or empty "
    Forth parameter is "the repo name for updates check" - use specific repo(comma separated)"

Example: ./$SCRIPT_NAME getInstalledDependency zip-2.0
Example: ./$SCRIPT_NAME getInstalledDependency zip-2.0
Example: ./$SCRIPT_NAME getInstalledDependency zip-2.0 refresh - with refresh repo metadata" 
Example: ./$SCRIPT_NAME getInstalledDependency zip-2.0 refresh teye - with refresh repo metadata and specific repo" 
