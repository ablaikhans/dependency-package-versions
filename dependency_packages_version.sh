#!/bin/bash

SCRIPT_NAME=`basename "$0"`
ACTION=`echo "$1" | tr '[:upper:]' '[:lower:]'` # ARG[1] is ACTION
DEPENDENCY=$2 # ARG[2] is package name with its version. Example: zip-3.0 
DEPENDENCY_PKG_NAME="" # package name without its version. Example: zip
INSTALLED_DEPENDENCY=""  # installed package name with its version. Example: zip-3.0
IS_DEPENDENCY_INSTALLED=False
REFRESH_REPO_METADATA=False
SET_REPO_NAME=False
REPO_NAME="" 

showHelp(){
    echo "[INFO] Use this script with parameters: "
    echo "[INFO]    first parameter is \"action\" :  "
    echo "[INFO]       ACTIONS: getInstalledDependency, checkUpdates"
    echo "[INFO]    second parameter is \"dependency package name\" "
    echo "[INFO]    third parameter is \"refresh repo metadata before updates check\" - refresh or empty "
    echo "[INFO]    forth parameter is \"the repo name for updates check\" - use specific repo(or comma separated repos) "
    echo "[INFO]"
    echo "[INFO] Example: {script_path} getInstalledDependency zip-2.0" 
    echo "[INFO] Example: ./$SCRIPT_NAME getInstalledDependency zip-2.0 - with no refresh repo matadata" 
    echo "[INFO] Example: ./$SCRIPT_NAME getInstalledDependency zip-2.0 refresh - with refresh repo metadata" 
    echo "[INFO] Example: ./$SCRIPT_NAME getInstalledDependency zip-2.0 refresh teye - with refresh repo metadata and specific repo" 
}

ARGS=('')
for var in "$@"
do
    ARGS+=($var)
done

# ARG[3+] are user to set SET_REPO_NAME/REFRESH_REPO_METADATA options
for (( i=3; i<=${#ARGS[@]};i++ ))
do
    arg=`echo ${ARGS[$i]} | tr '[:upper:]' '[:lower:]'`
    if [[ $arg == "--refresh" ]]; then REFRESH_REPO_METADATA=True; fi
    if [[ $arg == "--repo" ]] ; then 
        SET_REPO_NAME=True
        REPO_NAME=${ARGS[$((i+1))]}
        if [[ -z $REPO_NAME ]]; then showHelp; fi
    fi
done

# make DEPENDENCY_PKG_NAME from DEPENDENCY (delete version from DEPENDENCY)
getDependencyPackegeNameFromDependencyName() {
    DASHES_COUNT_IN_DEPENDENCY=`echo $(($(echo $DEPENDENCY | sed 's/-/ /g' | wc -w) - 1))`
    
    if [ $DASHES_COUNT_IN_DEPENDENCY -eq 0 ]; then DEPENDENCY_PKG_NAME=$DEPENDENCY; 
    else
        tmp=`echo $DEPENDENCY | sed 's/-/ /g'`
        DEPENDENCY_NAME_LIKE_LIST=()
        for item in ${tmp[@]} 
        do
            DEPENDENCY_NAME_LIKE_LIST+=($item)
        done 

        for (( i=0; i<$DASHES_COUNT_IN_DEPENDENCY; i++ ))
        do  
            DEPENDENCY_PKG_NAME+=${DEPENDENCY_NAME_LIKE_LIST[$i]}
            if  [ $i -ne $(( DASHES_COUNT_IN_DEPENDENCY-1 )) ]
            then DEPENDENCY_PKG_NAME+="-"
            fi
        done
    fi
}

isPackageInstalled(){
    cmd=`dnf list installed $DEPENDENCY_PKG_NAME 2>&1 | grep "No matching Packages to list"`
    if [ -z "$cmd" ]; then IS_DEPENDENCY_INSTALLED=True;fi
}

getInstalledDependency(){
    getDependencyPackegeNameFromDependencyName
    isPackageInstalled
    if [ $IS_DEPENDENCY_INSTALLED == True ]
    then
        cmd=`dnf list installed $DEPENDENCY_PKG_NAME 2>/dev/null | grep $DEPENDENCY_PKG_NAME`

        INSTALLED_DEPENDENCY_PKG_NAME=`echo $cmd | awk -F"." '{ print $1}'`
        INSTALLED_DEPENDENCY_PKG_VERSION=`echo $cmd | awk '{print $2}'`
        INSTALLED_DEPENDENCY="${INSTALLED_DEPENDENCY_PKG_NAME}-${INSTALLED_DEPENDENCY_PKG_VERSION}"

        echo "Installed dependency: $INSTALLED_DEPENDENCY"
    else
        echo "Dependency - $DEPENDENCY_PKG_NAME - is not installed"
        exit 0
    fi
}

checkUpdates(){
    getDependencyPackegeNameFromDependencyName
    isPackageInstalled
    if [ $IS_DEPENDENCY_INSTALLED == True ]
    then
        refresh=""
        reponame=""
        if  [ $REFRESH_REPO_METADATA == True ] ; then refresh="--refresh"  ; fi
        if  [ $SET_REPO_NAME == True ] ; then  reponame="--repo=$REPO_NAME" ; fi
        #dependencyAvailableUpdateVersion=`dnf list $refresh $reponame updates $DEPENDENCY_PKG_NAME 2>/dev/null | grep $DEPENDENCY_PKG_NAME | awk '{ print $2 }'`
        if [ -z "$dependencyAvailableUpdateVersion" ] 
        then 
            echo "Package $DEPENDENCY_PKG_NAME already installed the latest version"
        else 
            echo "Has updates: $DEPENDENCY_PKG_NAME-$dependencyAvailableUpdateVersion"
        fi
    else
        echo "Dependency - $DEPENDENCY_PKG_NAME - is not installed"
        exit 0
    fi
}


if [[ $ACTION == "getinstalleddependency" ]] ; then getInstalledDependency
elif [[ $ACTION == "checkupdates" ]] ; then checkUpdates
else showHelp
fi
